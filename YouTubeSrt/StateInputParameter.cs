﻿namespace YouTubeSrt
{
    public static class StateInputParameter
    {
        public static bool IsRemoveCrossingTime { get; set; }
        public static bool IsRemoveHtmlTags { get; set; }
        public static bool IsMultipleSpaces { get; set; }
        public static bool IsReplaceChars { get; set; }
    }
}