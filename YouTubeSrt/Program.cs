﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using YouTubeXml.Parser;
using YouTubeXml.Parser.Utils;

namespace YouTubeSrt
{
    class Program
    {
        static void Main(string[] args)
        {
            var listHelpParam = new List<string> {"help", "--help", "-h", "/?", null};
            var listKeys = new List<string>();

            if (args.Length == 0 || listHelpParam.Any(argHelp => args[0].ToLower().Equals(argHelp)))
            {
                PrintHelpScreen();
                return;
            }

            foreach (var s in args)
            {
                if (s.ToLower().Equals("--remove-cross-time")) StateInputParameter.IsRemoveCrossingTime = true;
                else if (s.ToLower().Equals("-r")) StateInputParameter.IsRemoveCrossingTime = true;
                else if (s.ToLower().Equals("--remove-html-tags")) StateInputParameter.IsRemoveHtmlTags = true;
                else if (s.ToLower().Equals("-h")) StateInputParameter.IsRemoveHtmlTags = true;
                else if (s.ToLower().Equals("--remove-multiple-spaces")) StateInputParameter.IsMultipleSpaces = true;
                else if (s.ToLower().Equals("-m")) StateInputParameter.IsMultipleSpaces = true;
                else if (s.ToLower().Equals("--remove-replace-chars")) StateInputParameter.IsReplaceChars = true;
                else if (s.ToLower().Equals("-c")) StateInputParameter.IsReplaceChars = true;
                else continue;

                listKeys.Add(s);
            }

            foreach (var inputFileName in args)
            {
                var isDirectory = Directory.Exists(inputFileName);
                var isFile = File.Exists(inputFileName);

                if (!isDirectory && !isFile && !listKeys.Any(key => inputFileName.Equals(key)))
                {
                    Console.WriteLine("File or directory '{0}' doesn't exists.", inputFileName);
                    continue;
                }

                if (isDirectory)
                {
                    Console.WriteLine("Processing directory '{0}'", inputFileName);
                    var dir = new DirectoryInfo(inputFileName);
                    foreach (var file in dir.GetFiles("*.xml"))
                    {
                        Console.WriteLine("Processing file '{0}'", file);
                        try
                        {
                            ConverteXml2Srt(file.FullName);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }

                if (isFile)
                {
                    try
                    {
                        ConverteXml2Srt(inputFileName);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        private static void PrintHelpScreen()
        {
            Console.WriteLine("YouTubeSrt.exe subtitle xmlv2 to srt сonverter.");
            Console.WriteLine();
            Console.WriteLine("Optional Arguments:");
            Console.WriteLine("\t -h, --remove-html-tags\t\t- Remove all html tags");
            Console.WriteLine("\t -r, --remove-cross-time\t\t- Correct the time intersections of text blocks");
            Console.WriteLine("\t -m, --remove-multiple-spaces\t\t- Remove all multiple spaces");
            Console.WriteLine("\t -c, --replace-chars\t\t- Replace all code to chars");
            Console.WriteLine("Usage examples:");
            Console.WriteLine("\tYouTubeSrt.exe subtitle.xml\t\t- converte xml subtitle file to srt");
            Console.WriteLine("\tYouTubeSrt.exe subtitles\t\t- сonverts all xml subtitles files in a folder to srt");
        }

        static void ConverteXml2Srt(string stringInput)
        {
            var reader = ParseYouTubeXml(stringInput);
            if (reader == null) return;

            var str = CreateString(reader);

            SaveSrt(str, stringInput);
        }

        static Reader ParseYouTubeXml(string inputFileName)
        {
            using (var fileStream = File.OpenRead(inputFileName))
            {
                var reader = new Reader(fileStream);

                reader.Read();

                return reader;
            }
        }

        static string CreateString(Reader reader)
        {
            if (StateInputParameter.IsRemoveCrossingTime) reader.RemoveCrossingTime();
            if (StateInputParameter.IsRemoveHtmlTags) reader.RemoveHtmlTags();
            if (StateInputParameter.IsMultipleSpaces) reader.RemoveMultipleSpaces();
            if (StateInputParameter.IsReplaceChars) reader.ReplaceChars();

            return ParametersConverter.Parse(reader.Parameters);
        }

        private static void SaveSrt(string str, string fileName)
        {
            var newFileName = fileName + ".srt";
            
            if (File.Exists(newFileName))
            {
                File.Delete(newFileName);
            }

            using (var fileStream = File.OpenWrite(newFileName))
            using (var writer = new StreamWriter(fileStream))
            {
                writer.Write(str);
                writer.Flush();
            }
        }
    }
}
