﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace YouTubeXml.Parser.Models
{
    public class Parameter : IComparable<Parameter>
    {
        /// <summary>
        /// Start Time
        /// </summary>
        public int T { get; private set; }

        /// <summary>
        /// Duration
        /// </summary>
        public int D { get; private set; }

        /// <summary>
        /// Uncknown
        /// </summary>
        public bool W { get; private set; }

        /// <summary>
        /// String
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Continue string
        /// </summary>
        public bool AppEnd { get; private set; }

        public Parameter(int t, int d, bool w, string value, bool appEnd = true)
        {
            T = t;
            D = d;
            W = w;
            Value = value;
            AppEnd = appEnd;
        }

        public int CompareTo(Parameter other)
        {
            if (T > other.T)
            {
                return 1;
            }

            if (T < other.T)
            {
                return -1;
            }

            return 0;
        }

        public static List<Parameter> ReadList(Stream stream)
        {
            var result = new List<Parameter>();

            var doc  = new XmlDocument();
            doc.Load(stream);

            var timedtext = doc.SelectSingleNode("timedtext");

            if (timedtext != null)
            {
                foreach (XmlNode childNode in timedtext.SelectNodes("text"))
                {
                    if (childNode.Attributes == null) continue;

                    int t = childNode.Attributes["t"] != null ? int.Parse(childNode.Attributes["t"].Value) : -1;
                    int d = childNode.Attributes["d"] != null ? int.Parse(childNode.Attributes["d"].Value) : -1;
                    bool w = childNode.Attributes["w"] != null && childNode.Attributes["w"].Value.Equals("1");
                    bool append = childNode.Attributes["append"] != null && childNode.Attributes["append"].Value.Equals("1");
                    string value = childNode.InnerText;

                    result.Add(new Parameter(t, d, w, value, append));
                }
            }

            return result;
        }
    }
}
