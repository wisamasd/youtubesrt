﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeXml.Parser.Models;

namespace YouTubeXml.Parser
{
    public class Reader
    {
        private readonly Stream _stream;

        public List<Parameter>  Parameters { get; set; }

        public Reader(Stream stream)
        {
            _stream = stream;
        }

        public void Read()
        {
            Parameters = Parameter.ReadList(_stream);
        }
    }
}
