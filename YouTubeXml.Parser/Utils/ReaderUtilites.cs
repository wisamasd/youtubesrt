﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using YouTubeXml.Parser.Models;

namespace YouTubeXml.Parser.Utils
{
    public static class ReaderUtilites
    {
        public static void RemoveCrossingTime(this Reader reader)
        {
            var parameters = reader.Parameters;
            var result = new List<Parameter>();

            if (parameters.Count < 1) return;

            for (int i = 0; i < parameters.Count; i++)
            {
                if (i != 0)
                {
                    var last = result.LastOrDefault();
                    var current = parameters[i];

                    if (last == null) continue;
                    if (current == null) continue;

                    var lastEndTime = last.T + last.D;

                    if (current.T < lastEndTime)
                    {
                        last.Value = last.Value + " " + current.Value;
                        continue;
                    }
                }

                result.Add(parameters[i]);
            }

            reader.Parameters = result;
        }

        public static void RemoveHtmlTags(this Reader reader)
        {
            foreach (var readerParameter in reader.Parameters)
            {
                readerParameter.Value = Regex.Replace(readerParameter.Value, "<[^>]+>", string.Empty);
            }
        }

        public static void RemoveMultipleSpaces(this Reader reader)
        {
            foreach (var readerParameter in reader.Parameters)
            {
                readerParameter.Value = Regex.Replace(readerParameter.Value, @"\s+", " ");
            }
        }

        public static void ReplaceChars(this Reader reader)
        {
            foreach (var readerParameter in reader.Parameters)
            {
                readerParameter.Value = Regex.Replace(readerParameter.Value, @"&#39;", "'");
            }
        }
    }
}
