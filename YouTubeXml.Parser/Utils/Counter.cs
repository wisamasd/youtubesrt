﻿namespace YouTubeXml.Parser.Utils
{
    public class Counter
    {
        public long Count { get; private set; }

        public Counter()
        {
            Reset();
        }

        public void Reset()
        {
            Count = 0;
        }

        public void Inc()
        {
            Count++;
        }
    }
}
