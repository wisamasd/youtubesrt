﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTubeXml.Parser.Models;

namespace YouTubeXml.Parser.Utils
{
    public class ParametersConverter
    {
        private static readonly StringBuilder StringBuilder = new StringBuilder();
        private static readonly Counter Counter = new Counter();

        public static string Parse(List<Parameter> descriptor)
        {
            if (descriptor.Count == 0) return "";

            StringBuilder.Clear();
            descriptor.Sort();
            Counter.Reset();

            _parse(descriptor);

            return StringBuilder.ToString();
        }

        private static void _parse(List<Parameter> descriptor)
        {
            foreach (var param in descriptor)
            {
                if (param.D <= 0) continue;
                if (param.T <= 0) continue;
                if (string.IsNullOrWhiteSpace(param.Value)) continue;

                Counter.Inc();
                var s = TimeSpan.FromMilliseconds(param.T);
                var e = TimeSpan.FromMilliseconds(param.T + param.D);
                var v = param.Value;

                StringBuilder.Append(Counter.Count);
                StringBuilder.Append(Environment.NewLine);
                StringBuilder.AppendFormat("{0}:{1}:{2},{3} --> {4}:{5}:{6},{7}", s.Hours, s.Minutes, s.Seconds, s.Milliseconds, e.Hours, e.Minutes, e.Seconds, e.Milliseconds);
                StringBuilder.Append(Environment.NewLine);
                StringBuilder.Append(v);
                StringBuilder.Append(Environment.NewLine);
                StringBuilder.Append(Environment.NewLine);
            }
        }
    }
}
