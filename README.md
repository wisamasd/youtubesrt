# README #

This utilite converts xml subtitles from YouTube (formate version 2) to *.srt

Optional Arguments:

    -h, --remove-html-tags       - Remove all html tags
    -r, --remove-cross-time      - Correct the time intersections of text blocks
    -m, --remove-multiple-spaces - Remove all multiple spaces
    -c, --replace-chars          - Replace all code to chars
Usage examples:
    
    YouTubeSrt.exe subtitle.xml  - converte xml subtitle file to srt;
    YouTubeSrt.exe subtitles     - сonverts all xml subtitles files in a folder to srt;